package com.app.wa.exception;

public class UserServiceException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5900949668283105795L;

	public UserServiceException(String message) {
		super(message);
	}
}

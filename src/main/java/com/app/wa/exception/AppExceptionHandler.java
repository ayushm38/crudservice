package com.app.wa.exception;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.app.wa.model.ErrorMessage;

@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(value = {Exception.class})
	public ResponseEntity<Object> handleAnyException(Exception ex, WebRequest request){
		return new ResponseEntity<>(ex, new HttpHeaders(),HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(value = {NullPointerException.class, UserServiceException.class})
	public ResponseEntity<Object> handleSpecificException(Exception ex, WebRequest request){
		String errorMessage =ex.getLocalizedMessage();
		if(errorMessage==null) errorMessage = ex.toString();
		ErrorMessage error = new ErrorMessage(new Date(), errorMessage);
		return new ResponseEntity<>(error, new HttpHeaders(),HttpStatus.EXPECTATION_FAILED);
	}
//	
//	@ExceptionHandler(value = {UserServiceException.class})
//	public ResponseEntity<Object> handleUserServiceException(UserServiceException ex, WebRequest request){
//		String errorMessage =ex.getLocalizedMessage();
//		if(errorMessage==null) errorMessage = ex.toString();
//		ErrorMessage error = new ErrorMessage(new Date(), errorMessage);
//		return new ResponseEntity<>(error, new HttpHeaders(),HttpStatus.EXPECTATION_FAILED);
//	}

}

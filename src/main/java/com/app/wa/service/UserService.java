package com.app.wa.service;

import com.app.wa.model.UpdateUserDetails;
import com.app.wa.model.UserDetails;
import com.app.wa.model.UserRest;

public interface UserService {
	UserRest createUserData(UserDetails details);
	UserRest getUserData(String userId);
	UserRest updateUserData(String userId,UpdateUserDetails details);
	Boolean deleteUserData(String userId);
}

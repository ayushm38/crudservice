package com.app.wa.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.wa.model.UpdateUserDetails;
import com.app.wa.model.UserDetails;
import com.app.wa.model.UserRest;
import com.app.wa.service.UserService;
import com.app.wa.shared.Utils;

@Service
public class UserServiceImpl implements UserService {

	Map<String, UserRest> users;

	Utils utils;

	UserServiceImpl() {
	}

	@Autowired
	UserServiceImpl(Utils utils) {
		this.utils = utils;
	}

	@Override
	public UserRest createUserData(UserDetails details) {
		if (users == null)
			users = new HashMap<>();

		String userId = utils.generateUserId();
		UserRest user = new UserRest();
		user.setFirstName(details.getFirstName());
		user.setLastName(details.getLastName());
		user.setEmail(details.getEmail());
		user.setUserId(userId);
		users.put(userId, user);

		return user;
	}

	@Override
	public UserRest getUserData(String userId) {
		UserRest user = null;
		if (users != null) {
			if (users.containsKey(userId)) {
				user = users.get(userId);
			}
		}
		return user;
	}

	@Override
	public UserRest updateUserData(String userId, UpdateUserDetails details) {
		UserRest storedUser = null;
		if (users != null) {
			if (users.containsKey(userId)) {
				storedUser = users.get(userId);
				storedUser.setFirstName(details.getFirstName());
				storedUser.setLastName(details.getLastName());
				users.put(userId, storedUser);
			}
		}
		return storedUser;
	}

	@Override
	public Boolean deleteUserData(String userId) {
		Boolean flag = false;
		if (users != null) {
			if (users.containsKey(userId)) {
				users.remove(userId);
				flag = true;
			}
		}
		return flag;
	}

}
